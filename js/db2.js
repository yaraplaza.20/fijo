// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyARz05od1gKpl9GaZE8nL56v6yW1u2QLWE",
  authDomain: "proyectofinalhp.firebaseapp.com",
  projectId: "proyectofinalhp",
  storageBucket: "proyectofinalhp.appspot.com",
  messagingSenderId: "649610069711",
  appId: "1:649610069711:web:53bc61b4c79c1d07f619ba"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();


var numCodigo = 0;
var nombre = "";
var grupo = "";
var descrippcion = "";
var urlImag = "";



// listar productos
Listarproductos(nombre)
function Listarproductos() {
    const titulo= document.getElementById('titulo');
    const  section = document.getElementById('secArticulos')
    titulo.innerHTML= nombre; 
    
    
  const dbRef = refS(db, 'Productos');
  const tabla = document.getElementById('tablaProductos');
 
  onValue(dbRef, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
          const childKey = childSnapshot.key;

          const data = childSnapshot.val();
          
          section.innerHTML+= "<div class ='card'> " +
                     "<img  id='urlImag' src=' "+ data.urlImag + "'  alt=''  width='200'> "
                     + "<h1 id='marca'  class='title'>" + data.nombre+ ":" + data.grupo +"</h1> <br> "
                     + "<p  id='descripcion' class ='anta-regurar'>" + data.descrippcion 
                     +  "</p><br> <button> Mas Informacion </button> </div>"  ;           


      });
  }, { onlyOnce: true });
}

// Listarproductos("Albums");